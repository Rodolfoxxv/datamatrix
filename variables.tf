# ID do projeto do GCP
variable "project" {
  description = "ID do projeto do GCP"
  type        = string
  default     = "portfolioentrada"
}

# Região do GCP
variable "region" {
  description = "Região do GCP"
  type        = string
  default     = "us-central1"
}

# ID do dataset do BigQuery
variable "dataset_id" {
  description = "ID do dataset do BigQuery"
  type        = string
  default     = "financial_market"
}

variable "bucket_name" {
  description = "Nome do bucket do Cloud Storage"
  type        = string
  default     = "entradaportifolio"
}


