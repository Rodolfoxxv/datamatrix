# Configura o provedor do Google
provider "google" {
  project = var.project
  region  = var.region

}

# Cria um conjunto de dados no BigQuery
resource "google_bigquery_dataset" "dataset1" {
  dataset_id = var.dataset_id
  project    = var.project
}

# Cria um bucket no Cloud Storage
resource "google_storage_bucket" "bucket1" {
  name     = "entradaportifolio"
  project  = var.project
  location = "US"
}

# Cria um conjunto de dados no BigQuery
resource "google_bigquery_dataset" "dataset2" {
  dataset_id                  = "financial_market"
  friendly_name               = "Financial Market"
  description                 = "Um dataset para o projeto portfolioentrada"
  location                    = "US"
  default_table_expiration_ms = 3600000
  project                     = var.project
}

# Cria uma tabela no BigQuery
resource "google_bigquery_table" "table2" {
  dataset_id = google_bigquery_dataset.dataset2.dataset_id
  table_id   = "tb_market_dataset"
  schema = file("schemas/financial_stock_market_stock_market_dataset.json")
}

# Referencia um bucket no Cloud Storage
resource "google_storage_bucket" "bucket2" {
  name     = var.bucket_name
  project  = var.project
  location = "US"
}

# Cria um objeto de bucket no Cloud Storage para simular um diretório
resource "google_storage_bucket_object" "object" {
  name   = "finance/stocks/"
  bucket = google_storage_bucket.bucket2.name
  content = ""
}
