from google.cloud import bigquery
from google.cloud import storage
import csv

def minha_funcao(data, context):
    """Triggered by a change to a Cloud Storage bucket.
    Args:
         data (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    bucket_name = data['bucket']
    file_name = data['name']
    dataset_id = 'my_project.financial_market'
    table_id = 'tb_market_dataset'

    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = storage.Blob(file_name, bucket)
    content = blob.download_as_text()

    lines = content.split("\n")
    reader = csv.reader(lines)

    rows_to_insert = [row for row in reader]

    bigquery_client = bigquery.Client()
    table_ref = "{}.{}".format(dataset_id, table_id)

    errors = bigquery_client.insert_rows(table_ref, rows_to_insert)
    if errors:
        print('Errors:', errors)
    else:
        print('Loaded {} rows into {}:{}.'.format(len(rows_to_insert), dataset_id, table_id))


# #!/bin/bash
#
# # Cria um ambiente virtual Python
# python3 -m venv env
#
# # Ativa o ambiente virtual
# source env/bin/activate
#
# # Instala as bibliotecas necessárias
# pip install google-cloud-storage google-cloud-bigquery
#
# # Executa a função localmente
# python main.py
